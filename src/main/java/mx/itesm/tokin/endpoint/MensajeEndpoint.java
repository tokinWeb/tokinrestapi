package mx.itesm.tokin.endpoint;
import mx.itesm.tokin.domain.Mensaje;
import mx.itesm.tokin.service.MensajeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.net.URI;

@Component
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public class MensajeEndpoint {
    @Autowired
    private MensajeService mensajeService;

    @GET
    @Path("/mensaje/{uuid}")
    public Response getMensaje(@PathParam("uuid") String uuid){
        Optional<Mensaje> mensaje = mensajeService.get(uuid);
        Response response = null;
        if(mensaje.isPresent()) {
            response = Response.ok(mensaje.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/mensajeA/{idChaval}/{idChaval2}")
    public Response searchAll(@PathParam("idChaval") long id,  @PathParam("idChaval2") long id2){
        Optional<List<Mensaje>> users = mensajeService.listAll(id,id2);
        Response response;
        if(users.isPresent()) {
            response = Response.ok(users.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }
    @GET
    @Path("/mensaje")
    public Response search(@QueryParam("page") Integer page, @QueryParam("size") Integer size){
        Optional<List<Mensaje>> events = mensajeService.list(page, size);
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }
    @POST
    @Path("/mensaje")
    public Response createMensaje(Mensaje mensaje) throws URISyntaxException
    {
        Optional<Mensaje> userDB = mensajeService.insert(mensaje);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        /**
        if(mensaje.getUsuario_id() == null || mensaje.getUsuario_id_e()== null || mensaje.getTexto()== null) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        mensaje.setId(uuid);
        mensaje.setUuid(randomUUIDString);
        mensaje.setEstatus(true);
        //DB.put(mensaje.getUuid(), mensaje);
        return Response.status(201).contentLocation(new URI("/mensaje-management/"+randomUUIDString)).build();**/
    }

    @PUT
    @Path("mensaje/{uuid}")
    public Response mensajeLeido(@PathParam("uuid") String uuid, Mensaje mensaje) throws URISyntaxException
    {
        mensaje.setUuid(uuid);
        Optional<Mensaje> userDB = mensajeService.update(mensaje);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;

        //Mensaje temp = DB.get(uuid);
       /** if(mensaje == null) {
            return Response.status(404).build();
        }
        //temp.setId(mensaje.getId());
        //temp.setUuid(mensaje.getUuid());
        //temp.setTexto(mensaje.getTexto());
        //temp.setUsuario_id_e(mensaje.getUsuario_id_e());
        temp.setUsuario_id(mensaje.getUsuario_id());
         temp.setFecha_envio(mensaje.getFecha_envio());**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
       // return Response.noContent().build(); // nada mas para que no marque error
    }





    /**@POST
    @Path("/mensaje")
    public Response insert(Mensaje mensaje){
        // acá se mandaría a llamar el servicio del usuario para insertar a la base
        // se debe validar el usuario
        mensaje.setUuid("3ec9a644-893a-4346-a883-161e59571914");
        mensaje.setId((long) 2);
        return Response.ok(mensaje).build();
    }**/
}
