package mx.itesm.tokin.endpoint;

import mx.itesm.tokin.domain.Evento;
import mx.itesm.tokin.service.EventoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public class EventoEndpoint {
    @Autowired
    private EventoService eventService;

    @GET
    @Path("/event/{uuid}")
    public Response getEvento(@PathParam("uuid") String uuid){

        Optional<Evento> event = eventService.get(uuid);
        Response response = null;
        if(event.isPresent()) {
            response = Response.ok(event.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/event")
    public Response search(@QueryParam("page") Integer page, @QueryParam("size") Integer size){
        Optional<List<Evento>> events = eventService.list(page, size);
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/eventB")
    public Response searchB(){
        Optional<List<Evento>> events = eventService.listB();
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }
    @GET
    @Path("/eventAll")
    public Response searchAll(){
        Optional<List<Evento>> events = eventService.listAll();
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/eventsAll/{id}")
    public Response eventsolAll(@PathParam("id") long id){
        Optional<List<Evento>> events = eventService.listSol(id);
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/eventsAllAll/{id}")
    public Response eventAll(@PathParam("id") long id){
        Optional<List<Evento>> events = eventService.listEventAll(id);
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @POST
    @Path("/event")
    public Response createEvento(Evento event) throws URISyntaxException
    {
        Optional<Evento> userDB = eventService.insert(event);

        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;

       /** if(event.getUsuario_id_requester() == null || event.getUsuario_id()== null || event.getFecha_hora()== null) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        event.setId(uuid);
        event.setUuid(randomUUIDString);
        event.setEstatus(true);
        event.setUsuarios(event.getUsuario_id_requester());
        event.setUsuarios(event.getUsuario_id());
        event.setTipo(1);

        //DB.put(event.getUuid(), event);
        return Response.status(201).contentLocation(new URI("/event-management/"+randomUUIDString)).build();**/
    }

    @PUT
    @Path("event/a/{uuid}")
    public Response acceptEvento(@PathParam("uuid") String uuid) throws URISyntaxException
    {
        Optional<Evento> event = eventService.get(uuid);

        event.get().setTipo(2);
        Optional<Evento> userDB = eventService.update(event.get());

        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;



        //Evento temp = DB.get(uuid);
        /**if(event == null) {
            return Response.status(404).build();
        }
        //temp.setId(event.getId());
        //temp.setUuid(event.getUuid());
        //temp.setComentario(event.getTexto());
        //temp.setUsuario_id_e(event.getUsuario_id_e());
        /**temp.setUsuario_id(event.getUsuario_id());
         temp.setFecha_envio(event.getFecha_envio());
         event.setTipo(2);**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build();//// nada mas para que no marque error
    }
    @PUT
    @Path("event/r/{uuid}")
    public Response rejectEvento(@PathParam("uuid") String uuid) throws URISyntaxException
    {
        //Evento temp = DB.get(uuid);
        Optional<Evento> event = eventService.get(uuid);

        event.get().setTipo(3);
        Optional<Evento> userDB = eventService.update(event.get());

        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;

        //temp.setId(event.getId());
        //temp.setUuid(event.getUuid());
        //temp.setComentario(event.getTexto());
        //temp.setUsuario_id_e(event.getUsuario_id_e());
        /**temp.setUsuario_id(event.getUsuario_id());
         temp.setFecha_envio(event.getFecha_envio());
         event.setTipo(0);**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }

    @PUT
    @Path("event/{uuid}")
    public Response updateEvento(@PathParam("uuid") String uuid, Evento event) throws URISyntaxException
    {
        //Evento temp = DB.get(uuid);
        event.setUuid(uuid);
        Optional<Evento> userDB = eventService.update(event);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //temp.setId(event.getId());
        //temp.setUuid(event.getUuid());
        //temp.setComentario(event.getTexto());
        //temp.setUsuario_id_e(event.getUsuario_id_e());
        /**temp.setUsuario_id(event.getUsuario_id());
         temp.setFecha_envio(event.getFecha_envio());**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }

    @PUT
    @Path("event/d/{uuid}")
    public Response deleteEvento(@PathParam("uuid") String uuid) throws URISyntaxException {
        //Evento temp = DB.get(uuid);
        Optional<Evento> event = eventService.get(uuid);

        event.get().setEstatus(0);
        Optional<Evento> userDB = eventService.update(event.get());

        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //temp.setId(event.getId());
        //temp.setUuid(event.getUuid());
        //temp.setComentario(event.getTexto());
        //temp.setUsuario_id_e(event.getUsuario_id_e());
        /**temp.setUsuario_id(event.getUsuario_id());
         temp.setFecha_envio(event.getFecha_envio());
         temp.setEstatus(false);**/

        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }



    /**@POST
    @Path("/event")
    public Response insert(Evento event){
        // acá se mandaría a llamar el servicio del usuario para insertar a la base
        // se debe validar el usuario
        event.setUuid("3ec9a644-893a-4346-a883-161e59571914");
        event.setId((long) 2);
        return Response.ok(event).build();
    }**/
}
