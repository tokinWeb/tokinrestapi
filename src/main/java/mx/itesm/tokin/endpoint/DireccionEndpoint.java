package mx.itesm.tokin.endpoint;

import mx.itesm.tokin.domain.Direccion;
import mx.itesm.tokin.service.DireccionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@Component
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public class DireccionEndpoint {
    @Autowired
    private DireccionService addressService;

    @GET
    @Path("/address/{uuid}")
    public Response getDireccion(@PathParam("uuid") String uuid){

        Optional<Direccion> address = addressService.get(uuid);
        Response response = null;
        if(address.isPresent()) {
            response = Response.ok(address.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/addressn/{direccion}")
    public Response getDireccionNombre(@PathParam("direccion") String direccion){

        Optional<Direccion> address = addressService.getNombre(direccion);
        Response response = null;
        if(address.isPresent()) {
            response = Response.ok(address.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/addressi/{longitud}/{latitud}")
    public Response getDireccionLangitud(@PathParam("longitud") String longitud,@PathParam("latitud") String latitud ){

        Optional<Direccion> address = addressService.getLangitud(longitud,latitud);
        Response response = null;
        if(address.isPresent()) {

            response = Response.ok(address.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/address")
    public Response search(@QueryParam("page") Integer page, @QueryParam("size") Integer size){
        Optional<List<Direccion>> addresss = addressService.list(page, size);
        Response response;
        if(addresss.isPresent()) {
            response = Response.ok(addresss.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }
    @POST
    @Path("/address")
    public Response createDireccion(Direccion address) throws URISyntaxException
    {
        Optional<Direccion> userDB = addressService.insert(address);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;

        /** if(address.getUsuario_id_requester() == null || address.getUsuario_id()== null || address.getFecha_hora()== null) {
         return Response.status(400).entity("Please provide all mandatory inputs").build();
         }
         UUID uuid = UUID.randomUUID();
         String randomUUIDString = uuid.toString();
         address.setId(uuid);
         address.setUuid(randomUUIDString);
         address.setEstatus(true);
         address.setUsuarios(address.getUsuario_id_requester());
         address.setUsuarios(address.getUsuario_id());
         address.setTipo(1);

         //DB.put(address.getUuid(), address);
         return Response.status(201).contentLocation(new URI("/address-management/"+randomUUIDString)).build();**/
    }



    @PUT
    @Path("address/{uuid}")
    public Response updateDireccion(@PathParam("uuid") String uuid, Direccion address) throws URISyntaxException
    {
        //Direccion temp = DB.get(uuid);
        address.setUuid(uuid);
        Optional<Direccion> userDB = addressService.update(address);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //temp.setId(address.getId());
        //temp.setUuid(address.getUuid());
        //temp.setComentario(address.getTexto());
        //temp.setUsuario_id_e(address.getUsuario_id_e());
        /**temp.setUsuario_id(address.getUsuario_id());
         temp.setFecha_envio(address.getFecha_envio());**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }

    @PUT
    @Path("address/d/{uuid}")
    public Response deleteDireccion(@PathParam("uuid") String uuid) throws URISyntaxException {
        //Direccion temp = DB.get(uuid);
        Optional<Direccion> address = addressService.get(uuid);

        address.get().setEstatus(0);
        Optional<Direccion> userDB = addressService.update(address.get());

        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //temp.setId(address.getId());
        //temp.setUuid(address.getUuid());
        //temp.setComentario(address.getTexto());
        //temp.setUsuario_id_e(address.getUsuario_id_e());
        /**temp.setUsuario_id(address.getUsuario_id());
         temp.setFecha_envio(address.getFecha_envio());
         temp.setEstatus(false);**/

        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }
}
