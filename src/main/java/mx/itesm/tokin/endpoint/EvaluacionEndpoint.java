package mx.itesm.tokin.endpoint;

import mx.itesm.tokin.domain.Evaluacion;
import mx.itesm.tokin.service.EvaluacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public class EvaluacionEndpoint {
    @Autowired
    private EvaluacionService evalService;

    @GET
    @Path("/eval/{uuid}")
    public Response getEvaluacion(@PathParam("uuid") String uuid){
        Optional<Evaluacion> eval = evalService.get(uuid);
        Response response = null;
        if(eval.isPresent()) {
            response = Response.ok(eval.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/eval")
    public Response search(@QueryParam("page") Integer page, @QueryParam("size") Integer size){
        Optional<List<Evaluacion>> events = evalService.list(page, size);
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/evalAll")
    public Response search(){
        Optional<List<Evaluacion>> events = evalService.listAll();
        Response response;
        if(events.isPresent()) {
            response = Response.ok(events.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }
    @POST
    @Path("/eval")
    public Response createEvaluacion(Evaluacion eval) throws URISyntaxException
    {

        Optional<Evaluacion> userDB = evalService.insert(eval);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        /**
        if(eval.getUsuario_id() == null || eval.getUsuario_id_e()== null || eval.getComentario()== null) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        eval.setId(uuid);
        eval.setUuid(randomUUIDString);
        eval.setEstatus(true);
        //DB.put(eval.getUuid(), eval);
        return Response.status(201).contentLocation(new URI("/eval-management/"+randomUUIDString)).build();
         **/
    }

    @PUT
    @Path("eval/{uuid}")
    public Response updateEvaluacion(@PathParam("uuid") String uuid, Evaluacion eval) throws URISyntaxException
    {
        eval.setUuid(uuid);
        Optional<Evaluacion> userDB = evalService.update(eval);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //Evaluacion temp = DB.get(uuid);
       /** if(eval == null) {
            return Response.status(404).build();
        }
        //temp.setId(eval.getId());
        //temp.setUuid(eval.getUuid());
        //temp.setComentario(eval.getTexto());
        //temp.setUsuario_id_e(eval.getUsuario_id_e());
        /**temp.setUsuario_id(eval.getUsuario_id());
         temp.setFecha_envio(eval.getFecha_envio());**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }




    /**
    @POST
    @Path("/eval")
    public Response insert(Evaluacion eval){
        // acá se mandaría a llamar el servicio del usuario para insertar a la base
        // se debe validar el usuario
        eval.setUuid("3ec9a644-893a-4346-a883-161e59571914");
        eval.setId((long) 2);
        return Response.ok(eval).build();
    }
    **/
}
