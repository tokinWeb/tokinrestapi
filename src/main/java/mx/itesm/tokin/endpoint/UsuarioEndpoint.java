package mx.itesm.tokin.endpoint;

import mx.itesm.tokin.domain.Usuario;
import mx.itesm.tokin.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.net.URI;

@Component
@Path("/v1")
@Produces(MediaType.APPLICATION_JSON)
public class UsuarioEndpoint {
    @Autowired
    private UsuarioService userService;

    @GET
    @Path("/user/{uuid}")
    public Response getUsuario(@PathParam("uuid") String uuid){
        Optional<Usuario> user =userService.get(uuid);
        Response response = null;
        if(user.isPresent()) {
            response = Response.ok(user.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/userd/{id}")
    public Response getUsuarioId(@PathParam("id") long id){
        Optional<Usuario> user =userService.get(id);
        Response response = null;
        if(user.isPresent()) {
            response = Response.ok(user.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/user")
    public Response search(@QueryParam("page") Integer page, @QueryParam("size") Integer size){
        Optional<List<Usuario>> users = userService.list(page, size);
        Response response;
        if(users.isPresent()) {
            response = Response.ok(users.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/userR")
    public Response searchBar(){
        Optional<List<Usuario>> users = userService.listBar();
        Response response;
        if(users.isPresent()) {
            response = Response.ok(users.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/userA")
    public Response searchAll(){
        Optional<List<Usuario>> users = userService.listAll();
        Response response;
        if(users.isPresent()) {
            response = Response.ok(users.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }

    @GET
    @Path("/userB")
    public Response searchBand(){
        Optional<List<Usuario>> users = userService.listBanda();
        Response response;
        if(users.isPresent()) {
            response = Response.ok(users.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
    }
    @POST
    @Path("/user")
    public Response createUsuario(Usuario user) throws URISyntaxException
    {
        Optional<Usuario> userDB = userService.insert(user);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;

        /**if(user.getNombre() == null || user.getGenero()== null) {
            return Response.status(400).entity("Please provide all mandatory inputs").build();
        }
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        user.setId(uuid);
        user.setUuid(randomUUIDString);
        //DB.put(user.getUuid(), user);
        return Response.status(201).contentLocation(new URI("/user-management/"+randomUUIDString)).build();**/
    }

    @PUT
    @Path("user/{uuid}")
    public Response updateUsuario(@PathParam("uuid") String uuid, Usuario user) throws URISyntaxException
    {
        user.setUuid(uuid);
        Optional<Usuario> userDB = userService.update(user);
        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //Usuario temp = DB.get(uuid);
        //if(user == null) {
            //return Response.status(404).build();
        //}
        //temp.setId(user.getId());
        //temp.setUuid(user.getUuid());
        //temp.setName(user.getName());
        //temp.setCorreo(user.getCorreo());
        /**temp.setGenero(user.getGenero());
        temp.setTel_movil(user.getTel_movil());
        temp.setDireccion(user.getDireccion());
        temp.setContraseña(user.getContraseña());
        temp.setImagen(user.getImagen());
         temp.setEstatus(user.getEstatus());**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
       // return Response.noContent().build(); // nada mas para que no marque error
    }

    @PUT
    @Path("user/d/{uuid}")
    public Response deleteUsuario(@PathParam("uuid") String uuid){
        Optional<Usuario> user = userService.get(uuid);
        //Usuario temp = DB.get(uuid);
        user.get().setEstatus(0);
        Optional<Usuario> userDB = userService.update(user.get());

        Response response;
        if(userDB.isPresent()) {
            response = Response.ok(userDB.get()).build();
        }else{
            response = Response.noContent().build();
        }
        return response;
        //temp.setEstatus(false);Este es el importante

        //temp.setId(user.getId());
        //temp.setUuid(user.getUuid());
        //temp.setName(user.getName());
        //temp.setCorreo(user.getCorreo());
        /**temp.setGenero(user.getGenero());
         temp.setTel_movil(user.getTel_movil());
         temp.setDireccion(user.getDireccion());
         temp.setContraseña(user.getContraseña());
         temp.setImagen(user.getImagen());**/
        //DB.put(temp.getUuid(), temp);
        //return Response.status(200).entity(temp).build();
        //return Response.noContent().build(); // nada mas para que no marque error
    }



/**    @POST
    @Path("/user")
    public Response insert(Usuario user){
        // acá se mandaría a llamar el servicio del usuario para insertar a la base
        // se debe validar el usuario
        user.setUuid("3ec9a644-893a-4346-a883-161e59571914");
        user.setId((long) 2);
        return Response.ok(user).build();
    }**/

}
