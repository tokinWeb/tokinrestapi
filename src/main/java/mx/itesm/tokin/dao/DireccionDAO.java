package mx.itesm.tokin.dao;

import mx.itesm.tokin.domain.Direccion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class DireccionDAO {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(DireccionDAO.class);

    public Optional<Direccion> getByUuid(String uuid) {
        String sql = "SELECT * FROM direccion WHERE uuid=?";
        try {
            BeanPropertyRowMapper<Direccion> rowMapper = new BeanPropertyRowMapper<>(Direccion.class);
            Direccion address = jdbcTemplate.queryForObject(sql, rowMapper, uuid);
            logger.debug("Getting address with uuid: " + uuid);
            return Optional.of(address);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No address with uuid: " + uuid);
        }
        return Optional.empty();
    }

    public Optional<Direccion> getByName(String direccion) {
        String sql = "SELECT * FROM direccion WHERE direccion=?";
        try {
            BeanPropertyRowMapper<Direccion> rowMapper = new BeanPropertyRowMapper<>(Direccion.class);
            Direccion address = jdbcTemplate.queryForObject(sql, rowMapper, direccion);
            logger.debug("Getting address with direccion: " + direccion);
            return Optional.of(address);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No address with direccion: " + direccion);
        }
        return Optional.empty();
    }

    public Optional<Direccion> getByLl(String longitud,String latitud) {
        String sql = "SELECT * FROM direccion WHERE longitud=? AND latitud=?";
        try {
            BeanPropertyRowMapper<Direccion> rowMapper = new BeanPropertyRowMapper<>(Direccion.class);
            Direccion address = jdbcTemplate.queryForObject(sql, rowMapper,longitud, latitud);
            logger.debug("Getting address with latitud: " + latitud);
            return Optional.of(address);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No address with latitud: " + latitud);
        }
        return Optional.empty();
    }



    public Optional<Direccion> insert(Direccion address) {
        String newUuid = UUID.randomUUID().toString();
        try {
            jdbcTemplate.update(
                    "INSERT INTO direccion "
                            + " (id, latitud, longitud, uuid, direccion, estatus)"
                            + " VALUES (?,?,?,?,?,?)",
                    address.getId(), address.getLatitud(), address.getLongitud(), address.getUuid(),address.getDireccion(), address.getEstatus());
            logger.debug("Inserting address");
            return getByUuid(address.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not insert address");
            return Optional.empty();
        }
    }

    public Optional<Direccion> update(Direccion address){
        try {
            jdbcTemplate.update("UPDATE direccion SET " +
                            "id=?, latitud=?, longitud=?, direccion=?, estatus=? WHERE uuid=?",
                    address.getId(),  address.getLatitud(), address.getLongitud(),address.getDireccion(), address.getEstatus(), address.getUuid());
            logger.debug("Updating address: " + address.getUuid());
            return getByUuid(address.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not update address: " + address.getUuid());
            return Optional.empty();
        }
    }


    public Optional<List<Direccion>> list(Integer page, Integer size) {
        String sql = "SELECT * FROM direccion WHERE estatus != FALSE LIMIT ?, ?";
        try {
            List<Direccion> addresses = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Direccion.class), (page * size), size);
            logger.debug("Getting address list ");
            return Optional.of(addresses);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get address list ");
        }
        return Optional.empty();
    }

    public Optional<Direccion> delete(Direccion address){
        try {
            jdbcTemplate.update("UPDATE direccion SET " +
                            "id=?, latitud=?, longitud=?, direccion=?, estatus=? WHERE uuid=?",
                    address.getId(),address.getLatitud(), address.getLongitud(),address.getDireccion(),0, address.getUuid());
            logger.debug("Deleting address: " + address.getUuid());
            return getByUuid(address.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not erase address: " + address.getUuid());
            return Optional.empty();
        }
    }

}
