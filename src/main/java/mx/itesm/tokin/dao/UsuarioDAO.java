package mx.itesm.tokin.dao;

import mx.itesm.tokin.domain.Usuario;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class UsuarioDAO {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(UsuarioDAO.class);

    public Optional<Usuario> getByUuid(String uuid) {
        String sql = "SELECT * FROM usuario WHERE uuid=?";
        try {
            BeanPropertyRowMapper<Usuario> rowMapper = new BeanPropertyRowMapper<>(Usuario.class);
            Usuario user = jdbcTemplate.queryForObject(sql, rowMapper, uuid);
            logger.debug("Getting user with uuid: " + uuid);
            return Optional.of(user);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No user with uuid: " + uuid);
        }
        return Optional.empty();
    }

    public Optional<Usuario> getById(Long id) {
        String sql = "SELECT * FROM usuario WHERE id=?";
        try {
            BeanPropertyRowMapper<Usuario> rowMapper = new BeanPropertyRowMapper<>(Usuario.class);
            Usuario user = jdbcTemplate.queryForObject(sql, rowMapper, id);
            logger.debug("Getting user with uuid: " + id);
            return Optional.of(user);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No user with uuid: " + id);
        }
        return Optional.empty();
    }

    public Optional<List<Usuario>> listBanda() {

            String sql = "SELECT * FROM usuario WHERE estatus != 0 AND tipo = 3";
            try {
                List<Usuario> users = jdbcTemplate.query(sql,
                        new BeanPropertyRowMapper<>(Usuario.class));
                logger.debug("Getting user list ");
                return Optional.of(users);
            } catch (EmptyResultDataAccessException e) {
                e.printStackTrace();
                logger.debug("Could not get user list ");
            }
            return Optional.empty();
    }

    public Optional<List<Usuario>> listBar() {

        String sql = "SELECT * FROM usuario WHERE estatus != 0 AND tipo=2";
        try {
            System.out.println("Entre");
            List<Usuario> users = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Usuario.class));
            logger.debug("Getting user list ");
            return Optional.of(users);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get user list ");
        }
        return Optional.empty();
    }

    public Optional<List<Usuario>> listAll() {

        String sql = "SELECT * FROM usuario WHERE estatus != 0";
        try {
            System.out.println("Entre");
            List<Usuario> users = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Usuario.class));
            logger.debug("Getting user list ");
            return Optional.of(users);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get user list ");
        }
        return Optional.empty();
    }



    public Optional<Usuario> insert(Usuario user) {
        String newUuid = UUID.randomUUID().toString();
        try {
            jdbcTemplate.update(
                    "INSERT INTO usuario "
                            + " (id, tipo, nombre, genero, correo, tel_movil,"
                            + " password, fecha_registro, fecha_act, estatus, "
                            + " imagen,uuid, direccion_id)"
                            + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
                    user.getId(), user.getTipo(), user.getNombre(), user.getGenero(), user.getCorreo(), user.getTel_movil(),
                    user.getPassword(),Timestamp.from(Instant.now()), Timestamp.from(Instant.now()), user.getEstatus(),
                    user.getImagen(),user.getUuid(), user.getDireccion_id());
            logger.debug("Inserting user");
            return getByUuid(user.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not insert user");
            return Optional.empty();
        }
    }

    public Optional<Usuario> update(Usuario user){
        try {
            jdbcTemplate.update("UPDATE usuario SET " +
                            "id=?, tipo=?, nombre=?, genero=?, correo=?, tel_movil=?, fecha_act=?, estatus=?,imagen=?, direccion_id=? WHERE uuid=?",
                    user.getId(), user.getTipo(), user.getNombre(), user.getGenero(), user.getCorreo(), user.getTel_movil(),
                    Timestamp.from(Instant.now()), user.getEstatus(), user.getImagen(), user.getDireccion_id(), user.getUuid());
            logger.debug("Updating user: " + user.getUuid());
            return getByUuid(user.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not update user: " + user.getUuid());
            return Optional.empty();
        }
    }


    public Optional<List<Usuario>> list(Integer page, Integer size) {
        String sql = "SELECT * FROM usuario WHERE estatus != FALSE LIMIT ?, ?";
        try {
            List<Usuario> users = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Usuario.class), (page * size), size);
            logger.debug("Getting user list ");
            return Optional.of(users);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get user list ");
        }
        return Optional.empty();
    }

    public Optional<Usuario> delete(Usuario user){
        try {
            jdbcTemplate.update("UPDATE usuario SET " +
                            "id=?, tipo=?, nombre=?, genero=?, correo=?, tel_movil=?, fecha_act=?, estatus=?,imagen=?, direccion_id=? WHERE uuid=?",
                    user.getId(), user.getTipo(), user.getNombre(), user.getGenero(), user.getCorreo(), user.getTel_movil(),
                    Timestamp.from(Instant.now()), 0, user.getImagen(), user.getDireccion_id(), user.getUuid());
            logger.debug("Deleting user: " + user.getUuid());
            return getByUuid(user.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not erase user: " + user.getUuid());
            return Optional.empty();
        }
    }


}
