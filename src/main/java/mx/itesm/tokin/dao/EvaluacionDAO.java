package mx.itesm.tokin.dao;

import mx.itesm.tokin.domain.Evaluacion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public class EvaluacionDAO {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(EvaluacionDAO.class);

    public Optional<List<Evaluacion>> listAll() {
        String sql = "SELECT * FROM evento WHERE estatus != 0";
        try {
            List<Evaluacion> events = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evaluacion.class));
            logger.debug("Getting event list ");
            return Optional.of(events);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get event list ");
        }
        return Optional.empty();
    }
    public Optional<Evaluacion> getByUuid(String uuid) {
        String sql = "SELECT * FROM evaluacion WHERE uuid=?";
        try {
            BeanPropertyRowMapper<Evaluacion> rowMapper = new BeanPropertyRowMapper<>(Evaluacion.class);
            Evaluacion review = jdbcTemplate.queryForObject(sql, rowMapper, uuid);
            logger.debug("Getting review with uuid: " + uuid);
            return Optional.of(review);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No review with uuid: " + uuid);
        }
        return Optional.empty();
    }

    public Optional<Evaluacion> insert(Evaluacion review) {
        String newUuid = UUID.randomUUID().toString();
        try {
            jdbcTemplate.update(
                    "INSERT INTO evaluacion "
                            + " (id, estrellas, comentario, fecha_eval, uuid, usuario_id,"
                            + " usuario_id_e, estatus)"
                            + " VALUES (?,?,?,?,?,?,?,?)",
                    review.getId(), review.getEstrellas(), review.getComentario(), Timestamp.from(Instant.now()), review.getUuid(),review.getUsuario_id(),
                    review.getUsuario_id_e(), review.getEstatus());
            logger.debug("Inserting review");
            return getByUuid(review.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not insert review");
            return Optional.empty();
        }
    }

    public Optional<Evaluacion> update(Evaluacion review){
        try {
            jdbcTemplate.update("UPDATE evaluacion SET " +
                            "id=?, estrellas=?, comentario=?, fecha_eval=?, usuario_id=?, usuario_id_e=?, estatus=? WHERE uuid=?",
                    review.getId(), review.getEstrellas(),review.getComentario(),review.getFecha_eval(),
                    review.getUsuario_id(),review.getUsuario_id_e(),review.getEstatus(), review.getUuid());
            logger.debug("Updating review: " + review.getUuid());
            return getByUuid(review.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not update review: " + review.getUuid());
            return Optional.empty();
        }
    }


    public Optional<List<Evaluacion>> list(Integer page, Integer size) {
        String sql = "SELECT * FROM evaluacion WHERE estatus != FALSE LIMIT ?, ?";
        try {
            List<Evaluacion> reviews = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evaluacion.class), (page * size), size);
            logger.debug("Getting review list ");
            return Optional.of(reviews);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get review list ");
        }
        return Optional.empty();
    }



}
