package mx.itesm.tokin.dao;

import mx.itesm.tokin.domain.Mensaje;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository
public class MensajeDAO {
    @Autowired
    protected JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(MensajeDAO.class);

    public Optional<Mensaje> getByUuid(String uuid) {
        String sql = "SELECT * FROM mensaje WHERE uuid=?";
        try {
            BeanPropertyRowMapper<Mensaje> rowMapper = new BeanPropertyRowMapper<>(Mensaje.class);
            Mensaje message = jdbcTemplate.queryForObject(sql, rowMapper, uuid);
            logger.debug("Getting message with uuid: " + uuid);
            return Optional.of(message);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No message with uuid: " + uuid);
        }
        return Optional.empty();
    }

   /** public Optional<Mensaje> getById(Long id) {
        String sql = "SELECT * FROM usuario WHERE id=?";
        try {
            BeanPropertyRowMapper<Mensaje> rowMapper = new BeanPropertyRowMapper<>(Mensaje.class);
            Mensaje message = jdbcTemplate.queryForObject(sql, rowMapper, id);
            logger.debug("Getting message with uuid: " + id);
            return Optional.of(message);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No message with uuid: " + id);
        }
        return Optional.empty();
    }**/

   public Optional<List<Mensaje>> listAll(long id, long id2) {

       String sql = "SELECT * FROM mensaje WHERE estatus != 0 OR (usuario_id=? AND usuario_id_e=?) OR (usuario_id=? AND usuario_id_e=?)  ";
       try {
           List<Mensaje> users = jdbcTemplate.query(sql,
                   new BeanPropertyRowMapper<>(Mensaje.class),id,id2,id2,id);
           logger.debug("Getting user list ");
           return Optional.of(users);
       } catch (EmptyResultDataAccessException e) {
           e.printStackTrace();
           logger.debug("Could not get user list ");
       }
       return Optional.empty();
   }
    public Optional<Mensaje> insert(Mensaje message) {
        String newUuid = UUID.randomUUID().toString();
        try {
            jdbcTemplate.update(
                    "INSERT INTO mensaje "
                            + " (id, texto, fecha_envio, uuid, usuario_id, usuario_id_e,"
                            + " estatus)"
                            + " VALUES (?,?,?,?,?,?,?)",
                    message.getId(), message.getTexto(), Timestamp.from(Instant.now()), message.getUuid(), message.getUsuario_id(), message.getUsuario_id_e(),
                    message.getEstatus());
            logger.debug("Inserting message");
            return getByUuid(message.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not insert message");
            return Optional.empty();
        }
    }

    public Optional<Mensaje> update(Mensaje message){
        try {
            jdbcTemplate.update("UPDATE mensaje SET " +
                            "id=?, texto=?, usuario_id=?, usuario_id_e=?, estatus=? WHERE uuid=?",
                    message.getId(), message.getTexto(), message.getUsuario_id(), message.getUsuario_id_e(), 1, message.getUuid());
            logger.debug("Updating message: " + message.getUuid());
            return getByUuid(message.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not update message: " + message.getUuid());
            return Optional.empty();
        }
    }


    public Optional<List<Mensaje>> list(Integer page, Integer size) {
        String sql = "SELECT * FROM mensaje WHERE estatus != FALSE LIMIT ?, ?";
        try {
            List<Mensaje> messages = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Mensaje.class), (page * size), size);
            logger.debug("Getting message list ");
            return Optional.of(messages);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get message list ");
        }
        return Optional.empty();
    }



}
