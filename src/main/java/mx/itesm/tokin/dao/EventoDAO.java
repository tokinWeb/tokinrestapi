package mx.itesm.tokin.dao;

import mx.itesm.tokin.domain.Evento;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public class EventoDAO {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    private static final Logger logger = LoggerFactory.getLogger(EventoDAO.class);

    public Optional<Evento> getByUuid(String uuid) {
        String sql = "SELECT * FROM evento WHERE uuid=?";
        try {
            BeanPropertyRowMapper<Evento> rowMapper = new BeanPropertyRowMapper<>(Evento.class);
            Evento event = jdbcTemplate.queryForObject(sql, rowMapper, uuid);
            logger.debug("Getting event with uuid: " + uuid);
            return Optional.of(event);
        } catch (EmptyResultDataAccessException e) {
            logger.debug("No event with uuid: " + uuid);
        }
        return Optional.empty();
    }

    public Optional<Evento> insert(Evento event) {
        String newUuid = UUID.randomUUID().toString();
        try {
            jdbcTemplate.update(
                    "INSERT INTO evento "
                            + " (id, fecha_hora, duracion, fecha_creacion, uuid, direccion_id,"
                            + " usuario_id, usuario_id_b, usuario_id_r, usuario_id_requester, "
                            + " estatus,tipo)"
                            + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                    event.getId(),event.getFecha_hora(),event.getDuracion(), Timestamp.from(Instant.now()),event.getUuid(),event.getDireccion_id(),
                    event.getUsuario_id(), event.getUsuario_id_b(), event.getUsuario_id_r(), event.getUsuario_id_requester(),
                    event.getEstatus(),event.getTipo());
            logger.debug("Inserting event");
            return getByUuid(event.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not insert event");
            return Optional.empty();
        }
    }

    public Optional<Evento> update(Evento event){
        try {
            jdbcTemplate.update("UPDATE evento SET " +
                            "id=?, fecha_hora=?, duracion=?, direccion_id=?, usuario_id=?, usuario_id_b=?, usuario_id_r=?, usuario_id_requester=?, estatus=?,tipo=? WHERE uuid=?",
                    event.getId(), event.getFecha_hora(), event.getDuracion(), event.getDireccion_id(),event.getUsuario_id(),event.getUsuario_id_b(),
                    event.getUsuario_id_r(),event.getUsuario_id_requester(),event.getEstatus(),event.getTipo(), event.getUuid());
            logger.debug("Updating event: " + event.getUuid());
            return getByUuid(event.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not update event: " + event.getUuid());
            return Optional.empty();
        }
    }

    public Optional<List<Evento>> listAll() {
        String sql = "SELECT * FROM evento WHERE estatus != 0";
        try {
            List<Evento> events = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evento.class));
            logger.debug("Getting event list ");
            return Optional.of(events);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get event list ");
        }
        return Optional.empty();
    }

    public Optional<List<Evento>> listB() {
        String sql = "SELECT * FROM evento WHERE estatus != 0 AND tipo=3";
        try {
            List<Evento> events = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evento.class));
            logger.debug("Getting event list ");
            return Optional.of(events);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get event list ");
        }
        return Optional.empty();
    }

    public Optional<List<Evento>> listSol(long id) {
        String sql = "SELECT * FROM evento WHERE estatus != 0 AND tipo=1 AND usuario_id=?";
        try {
            List<Evento> events = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evento.class),id);
            logger.debug("Getting event list ");
            return Optional.of(events);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get event list ");
        }
        return Optional.empty();
    }

    public Optional<List<Evento>> listEvens(long id) {
        String sql = "SELECT * FROM evento WHERE estatus != 0 AND (usuario_id=? OR usuario_id_requester=?)";
        try {
            List<Evento> events = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evento.class),id,id);
            logger.debug("Getting event list ");
            return Optional.of(events);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get event list ");
        }
        return Optional.empty();
    }

    public Optional<List<Evento>> list(Integer page, Integer size) {
        String sql = "SELECT * FROM evento WHERE estatus != 0 LIMIT ?, ?";
        try {
            List<Evento> events = jdbcTemplate.query(sql,
                    new BeanPropertyRowMapper<>(Evento.class), (page * size), size);
            logger.debug("Getting event list ");
            return Optional.of(events);
        } catch (EmptyResultDataAccessException e) {
            e.printStackTrace();
            logger.debug("Could not get event list ");
        }
        return Optional.empty();
    }

    public Optional<Evento> delete(Evento event){
        try {
            jdbcTemplate.update("UPDATE evento SET " +
                            "id=?, fecha_hora=?, duracion=?, direccion_id=?, usuario_id=?, usuario_id_b=?, usuario_id_r=?, usuario_id_requester=?, estatus=?,tipo=? WHERE uuid=?",
                    event.getId(), event.getFecha_hora(), event.getDuracion(), event.getDireccion_id(),event.getUsuario_id_b(),
                    event.getUsuario_id_r(),event.getUsuario_id_requester(),0,event.getTipo(), event.getUuid());
            logger.debug("Deleting event: " + event.getUuid());
            return getByUuid(event.getUuid());
        } catch (Exception e) {
            e.printStackTrace();
            logger.debug("Could not erase event: " + event.getUuid());
            return Optional.empty();
        }
    }


}

