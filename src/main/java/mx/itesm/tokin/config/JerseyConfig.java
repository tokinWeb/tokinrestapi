package mx.itesm.tokin.config;

import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import mx.itesm.tokin.endpoint.CORSResponseFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.stereotype.Component;


@Component
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(JacksonJaxbJsonProvider.class);
        register(CORSResponseFilter.class);
        packages("mx.itesm.tokin.endpoint");
        property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
    }

}
