package mx.itesm.tokin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@SpringBootApplication
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class TokinApplication extends ResourceServerConfigurerAdapter {
	public static void main(String[] args) {
		SpringApplication.run(TokinApplication.class, args);
	}
	@Override
	public void configure(HttpSecurity http) throws Exception {
	    //http.antMatcher("/user").authorizeRequests().anyRequest().permitAll();
		http.antMatcher("/**").authorizeRequests().anyRequest().permitAll();

	}



}
