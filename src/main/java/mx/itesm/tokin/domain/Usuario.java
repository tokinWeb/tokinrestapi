package mx.itesm.tokin.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.UUID;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Usuario {//implements UserDetails {

    private Long id;

    private String uuid;

    private String nombre;

    private String genero;
    private String correo;

    private String tel_movil;


    private Integer tipo;
    private String password;

    private Integer estatus;

    private String imagen;
    private Long direccion_id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fecha_registro;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fecha_act;

    public Long getId() {
        return id;
    }



    public void setId(long id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String name) {
        this.nombre = name;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTel_movil() {
        return tel_movil;
    }

    public void setTel_movil(String tel_movil) {
        this.tel_movil = tel_movil;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public Long getDireccion_id() {
        return direccion_id;
    }

    public void setDireccion_id(Long idDirection) {
        this.direccion_id = idDirection;
    }

    public Date getFecha_registro() {
        return fecha_registro;
    }

    public void setFecha_registro(Date fecha_registro) {
        this.fecha_registro= fecha_registro;
    }

    public Date getFecha_act() {
        return fecha_act;
    }

    public void setFecha_act(Date dateModified) {
        this.fecha_act = dateModified;
    }
/**
    @Override
    public String getUsername() {
        return correo;
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        return authorities;
    }


    @Override
    public boolean isAccountNonExpired() {
        if (estatus == 0){
            return false;
        }else{return true;}
    }

    @Override
    public boolean isAccountNonLocked() {
        if (estatus == 0){
            return false;
        }else{return true;}
    }

    @Override
    public boolean isCredentialsNonExpired() {
        if (estatus == 0){
            return false;
        }else{return true;}
    }

    @Override
    public boolean isEnabled() {
        if (estatus == 0){
            return false;
        }else{return true;}
    }
**/
}

