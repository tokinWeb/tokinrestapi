package mx.itesm.tokin.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Evaluacion {
    private Long id;

    private String uuid;
    private Long usuario_id_e;
    private Long usuario_id;
    private String comentario;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fecha_eval;
    private Integer estrellas;
    private Integer estatus;

    public Integer getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas=estrellas;
    }
    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus=estatus;
    }
    public Long getId() {
        return id;
    }



    public void setId(long id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario=comentario;
    }

    public Date getFecha_envio() {
        return fecha_eval;
    }

    public Date getFecha_eval() {
        return fecha_eval;
    }
    public void setFecha_eval(Date fecha_eval) {
        this.fecha_eval = fecha_eval;
    }
   /**
    public void setFecha_eval() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("YYYY-MM-DD HH:MM:SS");

        Date today = new Date();

        Date todayWithZeroTime = formatter.parse(formatter.format(today));

        this.fecha_eval = todayWithZeroTime;
    }**/
    public Long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Long usuario_id) {
        this.usuario_id = usuario_id;
    }
    public Long getUsuario_id_e() {
        return usuario_id_e;
    }

    public void setUsuario_ide(Long usuario_id_e) {
        this.usuario_id_e = usuario_id_e;
    }

}
