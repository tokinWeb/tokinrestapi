package mx.itesm.tokin.domain;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Direccion {

    private Long id;

    private String uuid;
    private String latitud;
    private String longitud;
    private String direccion;
    private Integer estatus;

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus=estatus;
    }

    public Long getId() {
        return id;
    }


    public void setId(long id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String texto) {
        this.direccion= texto;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud= latitud;
    }
    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud= longitud;
    }
}


