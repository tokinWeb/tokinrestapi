package mx.itesm.tokin.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import mx.itesm.tokin.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public class Evento {
    private Long id;
    @Autowired
    private UsuarioService userService;

    private String uuid;

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date fecha_hora;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date fecha_creacion;
    private Integer duracion;
    private Long usuario_id_b;
    private Long usuario_id_r;
    private Long usuario_id_requester;
    private Long usuario_id;
    private Integer tipo;
    private Long direccion_id;

    private Integer estatus;

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus=estatus;
    }

    public Long getId() {
        return id;
    }



    public void setId(long id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getFecha_hora() {
        return fecha_hora;
    }

    public void setFecha_hora(Date fecha_hora) {
        this.fecha_hora = fecha_hora;
    }

    public Integer getDuracion() {
        return duracion;
    }
    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
    public Long getUsuario_id_b() {
        return usuario_id_b;
    }

    public void setUsuario_id_b(Long usuario_id_b) {
        this.usuario_id_b = usuario_id_b;
    }

    public Long getUsuario_id_r() {
        return usuario_id_r;
    }

    public void setUsuario_id_r(Long usuario_id_r) {
        this.usuario_id_r = usuario_id_r;
    }

    public void setUsuario_id_requester(Long usuario_id_requester) {
        this.usuario_id_requester = usuario_id_requester;
    }
    public Long getUsuario_id_requester() {
        return usuario_id_requester;
    }


    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public Long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Long usuario_id) {
        this.usuario_id = usuario_id;
    }

    public void setDireccion_id(Long direccion_id) {
        this.direccion_id = direccion_id;
    }

    public Long getDireccion_id() {
        return direccion_id;
    }


    public Date getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(Date fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**public void setFecha_creacion() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

        Date today = new Date();

        Date todayWithZeroTime = formatter.parse(formatter.format(today));

        this.fecha_creacion = todayWithZeroTime;
    }**/



}
