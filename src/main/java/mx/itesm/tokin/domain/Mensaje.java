package mx.itesm.tokin.domain;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Mensaje {
    private Long id;

    private String uuid;
    private Long usuario_id_e;
    private Long usuario_id;
    private String texto;

    @JsonFormat(pattern = "yyyy-MM-dd HH:MM:SS")
    private Date fecha_envio;
    private Integer estatus;

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus=estatus;
    }

    public Long getId() {
        return id;
    }



    public void setId(long id) {
        this.id = id;
    }
    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto= texto;
    }

    public Date getFecha_envio() {
        return fecha_envio;
    }

    public void setFecha_envio(Date fecha_envio) {
        this.fecha_envio = fecha_envio;
    }
   /** public void setFecha_envio() throws ParseException {
        DateFormat formatter = new SimpleDateFormat("YYYY-MM-DD HH:MM:SS");

        Date today = new Date();

        Date todayWithZeroTime = formatter.parse(formatter.format(today));

        this.fecha_envio = todayWithZeroTime;
    }**/
    public Long getUsuario_id() {
        return usuario_id;
    }

    public void setUsuario_id(Long usuario_id) {
        this.usuario_id = usuario_id;
    }

    public void setUsuario_id_e(Long usuario_id_e) {
        this.usuario_id_e = usuario_id_e;
    }

    public Long getUsuario_id_e() {
        return usuario_id_e;
    }



}
