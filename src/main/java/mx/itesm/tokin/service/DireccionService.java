package mx.itesm.tokin.service;

import mx.itesm.tokin.dao.DireccionDAO;
import mx.itesm.tokin.dao.UsuarioDAO;
import mx.itesm.tokin.domain.Direccion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Service
public class DireccionService {
    @Autowired
    private DireccionDAO addressDAO;
    @Autowired
    private UsuarioService usuarioService;

    public Optional<Direccion> get(String uuid) {
        // validar los datos y cualquier lógica de negocio
        // modificar el objeto o agregar datos

        Optional<Direccion> address = addressDAO.getByUuid(uuid);
        if (address.isPresent()) {
            return address;
        } else {
            return Optional.empty();
        }
    }

    public Optional<Direccion> getNombre(String nombre){
        Optional<Direccion> address = addressDAO.getByName(nombre);
        if (address.isPresent()) {
            return address;
        } else {
            return Optional.empty();
        }
    }

    public Optional<Direccion> getLangitud(String longitud, String latitud) {
        Optional<Direccion> address = addressDAO.getByLl(longitud,latitud);
        if (address.isPresent()) {
            System.out.println(address);
            return address;
        } else {
            return Optional.empty();
        }
    }
    public Optional<Direccion> insert(Direccion address) {
        // validar que el correo no existe por ejemplo
        // validar que vengan todos los campos necesarios
        if (address.getLongitud() == null || address.getLatitud() == null || address.getDireccion() == null){
            return Optional.empty();
        } else {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();

            address.setUuid(randomUUIDString);
            address.setEstatus(1);
            System.out.println("entre7");
            return addressDAO.insert(address);
        }
    }

    public Optional<Direccion> update(Direccion address) {
        // validar los datos y cualquier lógica de negocio
        Optional<Direccion> addressDB = addressDAO.getByUuid(address.getUuid());
        if (addressDB.isPresent()) {
            if (address.getDireccion() == null) {
                address.setDireccion(addressDB.get().getDireccion());
            }
            if (address.getLongitud() == null) {
                address.setLongitud(addressDB.get().getLongitud());
            }
            if (address.getLatitud() == null) {
                address.setLatitud(addressDB.get().getLatitud());
            }
            if (address.getEstatus() == null) {
                address.setEstatus(addressDB.get().getEstatus());
            }
            //Se debe de poner los passwords?
            return addressDAO.update(address);
        } else {
            return Optional.empty();
        }
    }

    public Optional<List<Direccion>> list(Integer page, Integer size) {
        // validar los datos y cualquier lógica de negocio
        return addressDAO.list(page, size);
    }
}