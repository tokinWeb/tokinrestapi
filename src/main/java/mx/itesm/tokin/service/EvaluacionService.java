package mx.itesm.tokin.service;

import mx.itesm.tokin.dao.EvaluacionDAO;
import mx.itesm.tokin.domain.Evaluacion;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class EvaluacionService {

    @Autowired
    private EvaluacionDAO reviewDAO;

    public Optional<Evaluacion> get(String uuid){
        // validar los datos y cualquier lógica de negocio
        // modificar el objeto o agregar datos

        Optional<Evaluacion> review = reviewDAO.getByUuid(uuid);
        if(review.isPresent()) {
            //review.get().setPassword(null);
            return review;
        }else{
            return Optional.empty();
        }
    }



    public Optional<Evaluacion> insert(Evaluacion review){
        // validar que el correo no existe por ejemplo
        // validar que vengan todos los campos necesarios
        if(review.getComentario()== null || review.getEstrellas()== null || review.getUsuario_id_e() == null || review.getUsuario_id()== null) {
            return Optional.empty();
        }else {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();

            review.setUuid(randomUUIDString);
            review.setEstatus(1);
            return reviewDAO.insert(review);
        }
    }

    public Optional<Evaluacion> update(Evaluacion review) {
        // validar los datos y cualquier lógica de negocio
        Optional<Evaluacion> reviewDB = reviewDAO.getByUuid(review.getUuid());
        if (reviewDB.isPresent()) {
            if (review.getComentario() == null) {
                review.setComentario(reviewDB.get().getComentario());
            }
            if (review.getEstatus() == null) {
                review.setEstatus(reviewDB.get().getEstatus());
            }
            if (review.getUsuario_id() == null) {
                review.setUsuario_id(reviewDB.get().getUsuario_id());
            }
            if (review.getUsuario_id_e() == null) {
                review.setUsuario_ide(reviewDB.get().getUsuario_id_e());
            }

            //Se debe de poner los passwords?
            return reviewDAO.update(review);
        } else {
            return Optional.empty();
        }
    }
    public Optional<List<Evaluacion>> list(Integer page, Integer size){
        // validar los datos y cualquier lógica de negocio
        return reviewDAO.list(page, size);
    }

    public Optional<List<Evaluacion>> listAll(){
        // validar los datos y cualquier lógica de negocio
        return reviewDAO.listAll();
    }

    /**
    private final ConcurrentMap<String, Evaluacion> evals;

    public EvaluacionService() {
        this.evals = new ConcurrentHashMap<>();
    }

    public Collection<Evaluacion> getAllEvaluaciones() {
        Collection<Evaluacion> allEvaluaciones = evals.values();
        return allEvaluaciones.isEmpty() ? Collections.emptyList() : new ArrayList<>(allEvaluaciones);
    }

    public Evaluacion getEvaluacion(String uuid) {
        return evals.get(uuid);
    }
    public Optional<Evaluacion> getEvaluacions(String uuid){
        //acá se mandaría a llamar el dao
        Evaluacion eval = new Evaluacion();
        eval.setId((long) 100000.0);
        eval.setUuid(uuid);
        eval.setComentario("Que pox");

        return Optional.of(eval);
    }**/
}
