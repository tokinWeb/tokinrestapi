package mx.itesm.tokin.service;
import mx.itesm.tokin.dao.MensajeDAO;
import mx.itesm.tokin.domain.Mensaje;


import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class MensajeService {
   // private final ConcurrentMap<String, Mensaje> mensajes;
    @Autowired
    private MensajeDAO messageDAO;

    public Optional<Mensaje> get(String uuid){
        // validar los datos y cualquier lógica de negocio
        // modificar el objeto o agregar datos

        Optional<Mensaje> message = messageDAO.getByUuid(uuid);
        if(message.isPresent()) {
            //message.get().setPassword(null);
            return message;
        }else{
            return Optional.empty();
        }
    }

    public Optional<List<Mensaje>> listAll(long id, long id2){
        // validar los datos y cualquier lógica de negocio
        return messageDAO.listAll(id,id2);
    }

    public Optional<Mensaje> insert(Mensaje message){
        // validar que el correo no existe por ejemplo
        // validar que vengan todos los campos necesarios
        if(message.getTexto() == null || message.getUsuario_id_e() == null || message.getUsuario_id()== null) {
            return Optional.empty();
        }else {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();

            message.setUuid(randomUUIDString);
            message.setEstatus(1);
            return messageDAO.insert(message);
        }
    }

    public Optional<Mensaje> update(Mensaje message) {
        // validar los datos y cualquier lógica de negocio
        Optional<Mensaje> messageDB = messageDAO.getByUuid(message.getUuid());
        if (messageDB.isPresent()) {
            if (message.getTexto() == null) {
                message.setTexto(messageDB.get().getTexto());
            }
            if (message.getEstatus() == null) {
                message.setEstatus(messageDB.get().getEstatus());
            }
            if (message.getUsuario_id() == null) {
                message.setUsuario_id(messageDB.get().getUsuario_id());
            }
            if (message.getUsuario_id_e() == null) {
                message.setUsuario_id_e(messageDB.get().getUsuario_id_e());
            }

            //Se debe de poner los passwords?
            return messageDAO.update(message);
        } else {
            return Optional.empty();
        }
    }
    public Optional<List<Mensaje>> list(Integer page, Integer size){
        // validar los datos y cualquier lógica de negocio
        return messageDAO.list(page, size);
    }

    /**
    public MensajeService() {
        this.mensajes = new ConcurrentHashMap<>();
    }

    public Collection<Mensaje> getAllMensajes() {
        Collection<Mensaje> allMensajes = mensajes.values();
        return allMensajes.isEmpty() ? Collections.emptyList() : new ArrayList<>(allMensajes);
    }

    public Mensaje getMensaje(String uuid) {
        return mensajes.get(uuid);
    }
    public Optional<Mensaje> getMensajes(String uuid){
        //acá se mandaría a llamar el dao
        Mensaje message = new Mensaje();
        message.setId((long) 100000.0);
        message.setUuid(uuid);
        message.setTexto("Que pox");

        return Optional.of(message);
    }**/
}
