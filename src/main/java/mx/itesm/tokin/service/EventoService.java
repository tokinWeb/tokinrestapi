package mx.itesm.tokin.service;

import mx.itesm.tokin.domain.Evento;
import mx.itesm.tokin.dao.EventoDAO;
import mx.itesm.tokin.domain.Usuario;
import mx.itesm.tokin.service.UsuarioService;

import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class EventoService {
    @Autowired
    private EventoDAO eventDAO;

    @Autowired
    private UsuarioService usuarioService;

    public Optional<Evento> get(String uuid){
        // validar los datos y cualquier lógica de negocio
        // modificar el objeto o agregar datos

        Optional<Evento> event = eventDAO.getByUuid(uuid);
        if(event.isPresent()) {
            return event;
        }else{
            return Optional.empty();
        }
    }

    public Optional<Evento> insert(Evento event){
        // validar que el correo no existe por ejemplo
        // validar que vengan todos los campos necesarios
        if(event.getFecha_hora() == null || event.getDuracion()== null || event.getDireccion_id()== null||  event.getUsuario_id_requester()== null || event.getUsuario_id()==null) {
            return Optional.empty();
        }else {
            UUID uuid = UUID.randomUUID();
            String randomUUIDString = uuid.toString();

            event.setUuid(randomUUIDString);
            //event.setUsuarios(event.getUsuario_id_requester());
            Optional<Usuario> user = usuarioService.get(event.getUsuario_id_requester());
            System.out.println(user.get().getTipo());
            if(user.get().getTipo()==3){
                event.setUsuario_id_b(event.getUsuario_id_requester());
            }else{
                if(user.get().getTipo()==2) {
                    event.setUsuario_id_r(event.getUsuario_id_requester());
                }
            }

            //event.setUsuarios(event.getUsuario_id());

            Optional<Usuario> user2 = usuarioService.get(event.getUsuario_id());
            System.out.println(user2.get().getTipo());
            if(user2.get().getTipo()==3){
                event.setUsuario_id_b(event.getUsuario_id_requester());
            }else{
                if(user2.get().getTipo()==2) {
                    event.setUsuario_id_r(event.getUsuario_id_requester());
                }
            }
            event.setTipo(1);
            event.setEstatus(1);
            return eventDAO.insert(event);
        }
    }

    public Optional<Evento> update(Evento event) {
        // validar los datos y cualquier lógica de negocio
        Optional<Evento> eventDB = eventDAO.getByUuid(event.getUuid());
        if (eventDB.isPresent()) {
            if (event.getFecha_hora() == null) {
                event.setFecha_hora(eventDB.get().getFecha_hora());
            }
            if (event.getTipo() == null) {
                event.setTipo(eventDB.get().getTipo());
            }
            if (event.getDuracion() == null) {
                event.setDuracion(eventDB.get().getDuracion());
            }
            if (event.getDireccion_id() == null) {
                event.setDireccion_id(eventDB.get().getDireccion_id());
            }
            if (event.getUsuario_id_b() == null) {
                event.setUsuario_id_b(eventDB.get().getUsuario_id_b());
            }
            if (event.getUsuario_id_r() == null) {
                event.setUsuario_id_r(eventDB.get().getUsuario_id_r());
            }
            if (event.getUsuario_id()== null) {
                event.setUsuario_id(eventDB.get().getUsuario_id());
            }
            if (event.getEstatus()== null) {
                event.setEstatus(eventDB.get().getEstatus());
            }
            //Se debe de poner los passwords?
            return eventDAO.update(event);
        } else {
            return Optional.empty();
        }
    }
    public Optional<List<Evento>> list(Integer page, Integer size){
        // validar los datos y cualquier lógica de negocio
        return eventDAO.list(page, size);
    }
    public Optional<List<Evento>> listB(){
        // validar los datos y cualquier lógica de negocio
        return eventDAO.listB();
    }
    public Optional<List<Evento>> listAll(){
        // validar los datos y cualquier lógica de negocio
        return eventDAO.listAll();
    }

    public Optional<List<Evento>> listSol(long id){
        // validar los datos y cualquier lógica de negocio
        return eventDAO.listSol(id);
    }

    public Optional<List<Evento>> listEventAll(long id){
        // validar los datos y cualquier lógica de negocio
        return eventDAO.listEvens(id);
    }
}
