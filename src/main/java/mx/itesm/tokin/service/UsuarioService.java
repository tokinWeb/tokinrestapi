package mx.itesm.tokin.service;

import mx.itesm.tokin.domain.Usuario;
import mx.itesm.tokin.dao.UsuarioDAO;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioDAO userDAO;

    public Optional<Usuario> get(String uuid){
        // validar los datos y cualquier lógica de negocio
        // modificar el objeto o agregar datos

        Optional<Usuario> user = userDAO.getByUuid(uuid);
        if(user.isPresent()) {
            user.get().setPassword(null);
            return user;
        }else{
            return Optional.empty();
        }
    }

    public Optional<Usuario> get(Long id){
        // validar los datos y cualquier lógica de negocio
        // modificar el objeto o agregar datos

        Optional<Usuario> user = userDAO.getById(id);
        if(user.isPresent()) {
            user.get().setPassword(null);
            return user;
        }else{
            return Optional.empty();
        }
    }

    public Optional<Usuario> insert(Usuario user){
        // validar que el correo no existe por ejemplo
        // validar que vengan todos los campos necesarios
        if(user.getNombre() == null || user.getGenero()== null || user.getTipo() == null || user.getCorreo()== null || user.getTel_movil()== null|| user.getImagen()== null || user.getDireccion_id()== null ) {
            return Optional.empty();
        }else {

            UUID uuid = UUID.randomUUID();

            String randomUUIDString = uuid.toString();


            user.setUuid(randomUUIDString);

            user.setEstatus(1);

            user.setPassword(DigestUtils.sha1Hex(user.getPassword()));
            System.out.println("entre7");
            return userDAO.insert(user);
        }

    }

    public Optional<Usuario> update(Usuario user) {
        // validar los datos y cualquier lógica de negocio
        Optional<Usuario> userDB = userDAO.getByUuid(user.getUuid());
        if (userDB.isPresent()) {
            if (user.getNombre() == null) {
                user.setNombre(userDB.get().getNombre());
            }
            if (user.getTipo() == null) {
                user.setTipo(userDB.get().getTipo());
            }
            if (user.getCorreo() == null) {
                user.setCorreo(userDB.get().getCorreo());
            }
            if (user.getDireccion_id() == null) {
                user.setDireccion_id(userDB.get().getDireccion_id());
            }
            if (user.getGenero() == null) {
                user.setGenero(userDB.get().getGenero());
            }
            if (user.getImagen() == null) {
                user.setImagen(userDB.get().getImagen());
            }
            if (user.getTel_movil()== null) {
                user.setTel_movil(userDB.get().getTel_movil());
            }
            if (user.getEstatus()== null) {
                user.setEstatus(userDB.get().getEstatus());
            }
            //Se debe de poner los passwords?
            return userDAO.update(user);
        } else {
            return Optional.empty();
        }
    }
    public Optional<List<Usuario>> list(Integer page, Integer size){
        // validar los datos y cualquier lógica de negocio
        return userDAO.list(page, size);
    }

    public Optional<List<Usuario>> listBanda(){
        // validar los datos y cualquier lógica de negocio
        return userDAO.listBanda();
    }

    public Optional<List<Usuario>> listBar(){
        // validar los datos y cualquier lógica de negocio
        return userDAO.listBar();
    }

    public Optional<List<Usuario>> listAll(){
        // validar los datos y cualquier lógica de negocio
        return userDAO.listAll();
    }

/** public Collection<Usuario> getAllUsuarios() {
        Collection<Usuario> allUsuarios = usuarios.values();
        return allUsuarios.isEmpty() ? Collections.emptyList() : new ArrayList<>(allUsuarios);
    }

    public Usuario getUsuario(String uuid) {
        return usuarios.get(uuid);
    }
    public Usuario getUsuario(Long id) {
        return usuarios.get(id);
    }
    public Optional<Usuario> getUsuarios(String uuid){
        //acá se mandaría a llamar el dao
        Usuario user = new Usuario();
        user.setId((long) 100000.0);
        user.setUuid(uuid);
        user.setName("Nombre");

        return Optional.of(user);
    }**/


}

